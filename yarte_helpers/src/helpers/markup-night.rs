use v_htmlescape::escape;

use std::fmt::{self, Display, Formatter};

pub struct MarkupAsStr<T>(T) where T: Display;

pub trait AsStr {
    fn as_str(&self) -> &str;
}

impl<T> Display for MarkupAsStr<T> where T: Display {
    default fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl<T> Display for MarkupAsStr<T> where T: Display + AsStr {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        escape(self.0.as_str()).fmt(f)
    }
}

impl<T> From<T> for MarkupAsStr<T> where T: Display{
    fn from(t: T) -> MarkupAsStr<T> {
        MarkupAsStr(t)
    }
}

macro_rules! impl_as_str_string {
    ($($t:ty)+) => ($(
        impl AsStr for $t {
            #[inline]
            fn as_str(&self) -> &str {
                &self
            }
        }
    )+)
}

#[rustfmt::skip]
impl_as_str_string!(String &String &&String);

macro_rules! impl_as_str_str {
    ($($t:ty)+) => ($(
        impl AsStr for $t {
            #[inline]
            fn as_str(&self) -> &str {
                self
            }
        }
    )+)
}

#[rustfmt::skip]
impl_as_str_str!(&str &&str &&&str);
