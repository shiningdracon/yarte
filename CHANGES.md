# CHANGES

## [0.1.2] (2019-03-19)

### Added 

- Conditional expressions in compile-time to evaluator.

### Fixed 

- Parenthesis expressions.

- Variable name resolution for partials.

## [0.1.1] (2019-03-13)

### Added

- More debugging options in configure file. New option are `grid`, `header`, `paging`, and `short`.

## [0.1.0] (2019-03-11)

### First release
