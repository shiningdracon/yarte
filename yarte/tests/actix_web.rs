#![cfg(feature = "with-actix-web")]
#[macro_use]
extern crate actix_web;

use actix_http::HttpService;
use actix_http_test::TestServer;
use actix_web::{http, App, Responder};
use bytes::Bytes;
use yarte::Template;

#[derive(Template)]
#[template(path = "hello.hbs")]
struct HelloTemplate<'a> {
    name: &'a str,
}

#[get("/")]
pub fn index() -> impl Responder {
    HelloTemplate { name: "world" }
}

#[test]
fn test_actix_web() {
    let mut srv = TestServer::new(|| HttpService::new(App::new().service(index)));

    let req = srv.get("/");
    let mut response = srv.block_on(req.send()).unwrap();
    assert!(response.status().is_success());
    assert_eq!(
        response.headers().get(http::header::CONTENT_TYPE).unwrap(),
        "text/html"
    );

    let bytes = srv.block_on(response.body()).unwrap();
    assert_eq!(bytes, Bytes::from_static("Hello, world!".as_ref()));
}
